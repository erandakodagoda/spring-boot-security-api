package com.layoutglobel.service.doctor;


import com.layoutglobel.dao.doctor.DoctorRepository;
import com.layoutglobel.mailservice.EmailService;
import com.layoutglobel.model.doctor.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {

    @Autowired
    private DoctorRepository repository;
    @Autowired
    private EmailService service;


    public Doctor initDoctor(Doctor doctor){
        return repository.saveAndFlush(doctor);
    }

    public List<Doctor> getDoctors(){
        service.sendEMail();
        return repository.findAll();
    }
}
