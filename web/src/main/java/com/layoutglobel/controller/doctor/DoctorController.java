package com.layoutglobel.controller.doctor;



import com.layoutglobel.model.doctor.Doctor;
import com.layoutglobel.service.doctor.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DoctorController {

    @Autowired
    private DoctorService service;

    @GetMapping("/api/v1/doctors")
    public List<Doctor> getDoctors(){
        return service.getDoctors();
    }

    @PostMapping("/api/v1/doctors")
    public Doctor create(@RequestBody final Doctor doctor){
        return service.initDoctor(doctor);
    }


}
