import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.layoutglobel.*"})
@EntityScan(basePackages = {"com.layoutglobel.*"})
@EnableJpaRepositories(basePackages = {"com.layoutglobel.*"})
public class HMSApplication {

    public static void main(String[] args) {

        SpringApplication.run(HMSApplication.class);
    }
}
